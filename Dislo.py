from discord.ext import commands
import discord, asyncio, datetime, traceback, os

description = """
Dislo - Bot francais innovant
"""

initial_extensions = (
    'cogs.utils',
    'cogs.anime',
    'cogs.github',
    'cogs.math',
    'cogs.config'
)
    

class Dislo(commands.Bot):
    def __init__(self):
        super().__init__(command_prefix=commands.bot.when_mentioned_or("!"), description=description,
                         pm_help=None, help_attrs=dict(hidden=True),
                         owner_id=226748223006441472)

        self.version = "2.5.1 - Config Update"
        self.fileV = "1"
        #self.token = "NOPE" #RELEASE
        self.token = "NOPE" #DEVELOP


        for extension in initial_extensions:
            try:
                self.load_extension(extension)
            except Exception as e:
                print("ERREUR pendant le chargement de l'extension : "+extension)
                traceback.print_exc()

    async def on_command_error(self, error, ctx):
        if isinstance(error, commands.NoPrivateMessage):
            await ctx.message.author.send('Commande impossible dans les messages privés.')
        elif isinstance(error, commands.DisabledCommand):
            await ctx.message.author.send('Commande désactivé.')
        else:
            if "Command" in str(error) and "is not found" in str(error):
                pass
            elif "The check functions for " in str(error):
                if ctx.message.content.split(" ")[0] == "!anime" or ctx.message.content.split(" ")[0] == "!manga":
                    await self.send_message(ctx.message.channel, "Le module 'cogs.anime' est désactivé")
                elif ctx.message.content.split(" ")[0] == "!github":
                    await self.send_message(ctx.message.channel, "Le module 'cogs.github' est désactivé")
                elif ctx.message.content.split(" ")[0] == "!calcul" or ctx.message.content.split(" ")[0] == "!fonction":
                    await self.send_message(ctx.message.channel, "Le module 'cogs.math' est désactivé")
                else:
                    await self.send_message(ctx.message.channel, "Vous n'avez pas le role 'DisloPerm'")
            else:
                embed = discord.Embed(title=':x: Command Error', colour=0x992d22) #Dark Red
                embed.add_field(name='Error', value=error, inline = False)
                embed.add_field(name='Guild', value=ctx.message.server)
                embed.add_field(name='Channel', value=ctx.message.channel)
                embed.add_field(name='User', value=ctx.message.author, inline = False)
                embed.add_field(name='Message', value=ctx.message.clean_content, inline = False)
                embed.timestamp = datetime.datetime.utcnow()
                await self.send_message(self.get_channel("434360986485391362"), embed = embed)

    async def on_server_join(self, server):
        if os.path.isdir("serveurs/"+server.name):
            try:
                with open("serveurs/"+server.name+"/config.txt", "r") as fichier:
                    result = fichier.read().split("\n")[0]
            except:
                result = -1
            if result != self.fileV:
                self.createConfig(server.name)
            try:
                with open("serveurs/"+server.name+"/modules.txt", "r") as fichier:
                    ServerExtension = fichier.read().split("\n")
                find = True
                for i in ServerExtension:
                    if i.split(" : ")[0] in initial_extensions:
                        pass
                    else:
                        find = False
            except:
                find = False
            if find == False:
                self.createModule(server.name)
        else:
            os.mkdir("serveurs/"+server.name)
            self.createConfig(server.name)
            self.createModule(server.name)
        find = False
        for role in server.roles:
            if role.name == "DisloPerm":
                find = True
        if find == False:
            try:
                await self.create_role(server, name = "DisloPerm", mentionable = False, hoist = False)
                await self.send_message(server.owner, "[FR] \nDislo a été ajouté sur votre serveur et il a ajouté le role 'DisloPerm' \nMerci de faire en sorte que seul le staff ait ce role.")
                await self.send_message(server.owner, "[EN] \nDislo was added to your server and he added the role 'DisloPerm' \nThank you for doing the only role of the staff.")
            except Exception as e:
                embed = discord.Embed(title=':x: Perms Error', colour=0x992d22) #Dark Red
                embed.add_field(name='Guild', value=server.name)
                embed.add_field(name = "Error", value = e, inline = False)
                embed.timestamp = datetime.datetime.utcnow()
                await self.send_message(self.get_channel("434360986485391362"), embed = embed)
                await self.send_message(server.owner, "[FR] \nDislo a été ajouté sur votre serveur. Cependant, il n'a pas pu créer le role 'DisloPerm' \nMerci de créer ce role.")
                await self.send_message(server.owner, "[EN] \nDislo has been added to your server. However, he could not create the role 'DisloPerm' \nThank you for creating this role.")

    async def on_ready(self):
        if not hasattr(self, 'uptime'):
            self.uptime = datetime.datetime.utcnow()

        print('LANCER')
        for i in self.servers:
            if os.path.isdir(i.name):
                try:
                    with open("serveurs/"+i.name+"/config.txt", "r") as fichier:
                        result = fichier.read().split("\n")[0]
                except:
                    result = -1
                if result != self.fileV:
                    self.createConfig(i.name)
                try:
                    with open("serveurs/"+i.name+"/modules.txt", "r") as fichier:
                        ServerExtension = fichier.read().split("\n")
                    find = True
                    for j in ServerExtension:
                        if j.split(" : ")[0] in initial_extensions:
                            pass
                        else:
                            find = False
                except:
                    find = False
                if find == False:
                    self.createModule(i.name)
            else:
                os.mkdir(i.name)
                self.createConfig(i.name)
                self.createModule(i.name)

            find = False
            for role in i.roles:
                if role.name == "DisloPerm":
                    find = True
            if find == False:
                try:
                    await self.create_role(i, name = "DisloPerm", mentionable = False, hoist = False)
                    await self.send_message(i.owner, "[FR] \nDislo a été ajouté sur votre serveur et il a ajouté le role 'DisloPerm' \nMerci de faire en sorte que seul le staff ait ce role.")
                    await self.send_message(i.owner, "[EN] \nDislo was added to your server and he added the role 'DisloPerm' \nThank you for doing the only role of the staff.")
                except Exception as e:
                    embed = discord.Embed(title=':x: Perms Error', colour=0x992d22) #Dark Red
                    embed.add_field(name='Guild', value=i.name)
                    embed.add_field(name = "Error", value = e, inline = False)
                    embed.timestamp = datetime.datetime.utcnow()
                    await self.send_message(self.get_channel("434360986485391362"), embed = embed)
                    await self.send_message(i.owner, "[FR] \nDislo a été ajouté sur votre serveur. Cependant, il n'a pas pu créer le role 'DisloPerm' \nMerci de créer ce role.")
                    await self.send_message(i.owner, "[EN] \nDislo has been added to your server. However, he could not create the role 'DisloPerm' \nThank you for creating this role.")

    async def on_resumed(self):
        print('Reprise...')

    async def on_message(self, message):
        if message.author.bot:
            return
        check = False
        for cog in bot.cogs:
            go = False
            cog = bot.get_cog(cog)
            try:
                go = await cog.message(message)
                if go == True:
                    check = True
            except AttributeError:
                continue
        if check == False:
            await self.process_commands(message)

    async def close(self):
        await super().close()
        await self.session.close()

    def run(self):
        super().run(self.token, reconnect=True)

    def createConfig(self, serverName):
        with open("serveurs/"+serverName+"/config.txt", "w") as fichier:
            texte = self.fileV+"\n"
            texte += "bot_speech"
            fichier.write(texte)
    
    def createModule(self, serverName):
        with open("serveurs/"+serverName+"/modules.txt", "w") as fichier:
            texte = ""
            for i in initial_extensions:
                texte += i+" : True\n"
            fichier.write(texte[:-1])

bot = Dislo()
bot.run()
