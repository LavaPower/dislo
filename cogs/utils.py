from discord.ext import commands
import asyncio, discord

class Utils:
    """Commandes utilitaires."""

    def __init__(self, bot):
        self.bot = bot

    async def message(self, message):
        with open("serveurs/"+message.server.name+"/config.txt", "r") as fichier:
            channelName = fichier.read().split("\n")[1]
        if message.channel.name == channelName:
            msg = message.content.split(" ")
            if msg[0] == "all":
                for i in message.server.channels:
                    if i.type == discord.ChannelType.text:
                        text = ""
                        nb = 0
                        for y in msg[1:]:
                            if y == "\\n":
                                y = "\n"
                            nb+=1
                            if nb == len(msg)-1:
                                text+=y
                            else:
                                if y == "\n":
                                    text+=y
                                else:
                                    text+=y+" "
                        await self.bot.send_message(i, text)
                        await self.bot.send_message(message.channel, "Message envoyé à "+i.name)
                await self.bot.send_message(message.channel, "Tous les channels ont eu le message.")
            else:
                for i in message.server.channels:
                    if i.name == msg[0]:
                        if i.type == discord.ChannelType.text:
                            text = ""
                            nb = 0
                            for y in msg[1:]:
                                if y == "\\n":
                                    y = "\n"
                                nb+=1
                                if nb == len(msg)-1:
                                    text+=y
                                else:
                                    if y == "\n":
                                        text+=y
                                    else:
                                        text+=y+" "
                            await self.bot.send_message(i, text)
                            await self.bot.send_message(message.channel, "Message envoyé")
                        else:
                            await self.bot.send_message(message.channel, "Message non-envoyé, ce channel n'est pas un channel textuel.")
            return False

    @commands.command()
    async def ping(self):
        """Ping ! Pong !"""
        await self.bot.say("Pong ! Je suis là")

    @commands.command(pass_context=True)
    async def help(self, ctx):
        """Commande d'aide"""
        aide = discord.Embed(title="Dislo - Help System", description="Système d'assistance de Dislo", colour = 0x2ecc71)
        aide.set_footer(text="Dislo - Help System",icon_url="https://cdn4.iconfinder.com/data/icons/meBaze-Freebies/512/info.png")
        aide.add_field(name = "- !help", value = "```Commande actuelle```", inline = False)
        aide.add_field(name = "- !infobot", value = "```Informations sur moi```", inline = False)
        aide.add_field(name = "- !infoserver", value = "```Informations sur le serveur où on est```", inline = False)
        aide.add_field(name = "- !infome", value = "```Informations sur toi```", inline = False)
        aide.add_field(name = "- !ping",value = "```Pong !```", inline = False)
        aide.add_field(name = "- !anime <nom>",value = "```Information sur un anime```", inline = False)
        aide.add_field(name = "- !manga <nom>",value = "```Information sur un manga```", inline = False)
        aide.add_field(name = "- !github",value = "```Aide du système Github```", inline = False)
        aide.add_field(name = "- !calcul <calcul>", value = "```Donne le résultat d'un calcul```", inline = False)
        aide.add_field(name = "- !fonction <formule> <min|max> <nbPoint>",value = "```Affiche le graphique d'une fonction```", inline = False)
        await self.bot.send_message(ctx.message.author, embed=aide)
        for i in ctx.message.author.roles:
            if i.name == "DisloPerm":
                aide = discord.Embed(title="Dislo - Help Admin System", description="Système d'assistance pour admin de Dislo", colour = 0x2ecc71)
                aide.set_footer(text="Dislo - Help AdminSystem",icon_url="https://cdn4.iconfinder.com/data/icons/meBaze-Freebies/512/info.png")
                aide.add_field(name = "- !config", value = "```Aide du système de configuration```", inline = False)
                await self.bot.send_message(ctx.message.author, embed = aide)
        await self.bot.send_message(ctx.message.author, "Vous pouvez aussi rejoindre le serveur de support : http://discord.gg/CRNdJuB")
        await self.bot.say("Aide envoyé à "+ctx.message.author.mention)

    @commands.command()
    async def infobot(self):
        """Informations sur Dislo"""
        info = discord.Embed(title="Dislo", description="Bot Dislo", colour = 0x3498db)
        info.set_footer(text="Dislo",icon_url="https://cdn4.iconfinder.com/data/icons/meBaze-Freebies/512/info.png")
        info.add_field(name = "Créé par",value = "LavaPower")
        info.add_field(name = "Créé le", value = "30 ‎novembre ‎2017")
        info.add_field(name = "Version",value=self.bot.version)
        info.add_field(name = "Site", value = "http://lavapower.github.io/dislo", inline = False)
        info.add_field(name = "Serveur support", value = "http://discord.gg/CRNdJuB", inline = False)
        nbServers = 0
        for i in self.bot.servers:
            nbServers += 1
        info.add_field(name = "Nombre de serveurs", value = nbServers)
        nbMembers = 0
        for i in self.bot.get_all_members():
            if i.bot:
                pass
            else:
                nbMembers += 1
        info.add_field(name = "Nombre de membres", value = nbMembers)
        await self.bot.say(embed=info)

    @commands.command(pass_context=True)
    async def infoserver(self, ctx):
        server = ctx.message.server
        info = discord.Embed(title=server.name, description="Information du serveur "+server.name, colour = 0x3498db)
        info.set_footer(text="Dislo",icon_url="https://cdn4.iconfinder.com/data/icons/meBaze-Freebies/512/info.png")
        info.add_field(name = "Créé par",value = server.owner.name)
        info.add_field(name = "Créé le", value = str(server.created_at)[:10])
        info.add_field(name = "Crée à", value = str(server.created_at)[11:19])
        nbChannels = 0
        for i in server.channels:
            if i.type == discord.ChannelType.text or i.type == discord.ChannelType.voice:
                nbChannels += 1
        info.add_field(name = "Nombre de channels", value = nbChannels)
        listeEmojiTxt = ""
        for i in server.emojis:
            listeEmojiTxt += str(i)+" "
        info.add_field(name = "Emojis du serveur", value = listeEmojiTxt, inline = False)
        nbRole = 0
        for i in server.roles:
            if i.is_everyone:
                pass
            else:
                nbRole += 1
        info.add_field(name = "Nombre de roles", value = nbRole)
        nbMembers = 0
        nbBot = 0
        for i in server.members:
            if i.bot:
                nbBot+= 1
            nbMembers += 1
        info.add_field(name = "Nombre de membres", value = str(nbMembers)+" dont "+str(nbBot)+" bots")
        info.set_thumbnail(url = "https://media.discordapp.net/icons/"+str(server.id)+"/"+str(server.icon)+".jpg")
        await self.bot.say(embed=info)

    @commands.command(pass_context=True)
    async def infome(self, ctx):
        me = ctx.message.author
        info = discord.Embed(title=me.name, description="Information de "+me.name, colour = 0x3498db)
        info.set_footer(text="Dislo",icon_url="https://cdn4.iconfinder.com/data/icons/meBaze-Freebies/512/info.png")
        info.add_field(name = "A rejoins le serveur le", value = str(me.joined_at)[:10])
        info.add_field(name = "A rejoins le serveur à", value = str(me.joined_at)[11:19])
        listeRoleTxt = ""
        for i in me.roles:
            if i.is_everyone:
                pass
            else:
                listeRoleTxt += i.name + " | "
        if " | " in listeRoleTxt:
            listeRoleTxt = listeRoleTxt[:-3]
        info.add_field(name = "Roles", value = listeRoleTxt, inline = False)
        info.add_field(name = "Status", value = str(me.status).replace("dnd", "Do Not Disturb"))
        info.add_field(name = "Jeu joué", value = me.game)
        info.add_field(name = "Surnom", value = me.nick)
        if me.avatar_url == "":
            info.set_thumbnail(url = me.default_avatar_url)
        else:
            info.set_thumbnail(url = me.avatar_url)
        await self.bot.say(embed=info)
        
def setup(bot):
    bot.remove_command("help")
    bot.add_cog(Utils(bot))
