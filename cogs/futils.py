def checkDisloPerm(ctx):
    find = False
    for i in ctx.message.author.roles:
        if i.name == "DisloPerm":
            find = True
    return find

def checkModuleSet(ctx):
    if ctx.message.content.split(" ")[0] == "!anime" or ctx.message.content.split(" ")[0] == "!manga":
        module = "cogs.anime"
    elif ctx.message.content.split(" ")[0] == "!github":
        module = "cogs.github"
    elif ctx.message.content.split(" ")[0] == "!calcul" or ctx.message.content.split(" ")[0] == "!fonction":
        module = "cogs.math"
    else:
        module = "Unknown"
    if module == "Unknown":
        return False
    else:
        with open("serveurs/"+ctx.message.server.name+"/modules.txt", "r") as fichier:
            ExtensionServer = fichier.read().split("\n")
        etat = "Unknown"
        for i in ExtensionServer:
            if i.split(" : ")[0] == module:
                etat = i.split(" : ")[1]
        if etat == "False":
            return False
        else:
            return True