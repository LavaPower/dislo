from discord.ext import commands
import asyncio, discord
from cogs.futils import checkDisloPerm

class Command:
    """Commande personnalisé du serveur."""

    def __init__(self, bot):
        self.bot = bot

    
def setup(bot):
    bot.add_cog(Command(bot))
