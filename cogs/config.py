from discord.ext import commands
import asyncio, discord
from cogs.futils import checkDisloPerm

class Config:
    """Configuration du serveur."""

    def __init__(self, bot):
        self.bot = bot

    @commands.group(pass_context=True)
    @commands.check(checkDisloPerm)
    async def config(self, ctx):
        if ctx.invoked_subcommand is None:
            aide = discord.Embed(title="Dislo - Config System", description="Système Config de Dislo", colour = 0x2ecc71)
            aide.set_footer(text="Dislo - Config System",icon_url="https://cdn4.iconfinder.com/data/icons/meBaze-Freebies/512/info.png")
            aide.add_field(name = "- !config help", value = "```Commande actuelle```", inline = False)
            aide.add_field(name = "- !config SpeechChannel <nom>", value = "```Choisir le channel de speech du bot```", inline = False)
            aide.add_field(name = "- !config Module <nom|list> <True|False>", value = "```Activer / Désactiver un module```", inline = False)
            await self.bot.say(embed=aide)
        
    @config.command(pass_context=True)
    async def Module(self, ctx, name = "None", etat = "None"):
        if name == "None":
            await self.bot.say("Usage : !set Module <nom|liste> [<True|False>]")
        elif name == "list":
            await self.bot.say("Liste des extensions :")
            with open("serveurs/"+ctx.message.server.name+"/modules.txt", "r") as fichier:
                ExtensionServer = fichier.read().split("\n")
            for i in ExtensionServer:
                if i.split(" : ")[0] != "cogs.config" and i.split(" : ")[0] != "cogs.utils":
                    if i.split(" : ")[1] == "True":
                        await self.bot.say("- "+i.split(" : ")[0]+" : activé")
                    else:
                        await self.bot.say("- "+i.split(" : ")[0]+" : désactivé")
        elif etat != "True" and etat != "False":
            await self.bot.say("Usage : !set Module <nom|list> [<True|False>]")
        elif name == "cogs.config" or name == "cogs.utils":
            await self.bot.say("Désolé, les modules 'cogs.config' et 'cogs.utils' ne sont pas configurable")
        else:
            with open("serveurs/"+ctx.message.server.name+"/modules.txt", "r") as fichier:
                ExtensionServer = fichier.read().split("\n")
            find = False
            for i in range(len(ExtensionServer)):
                if ExtensionServer[i].split(" : ")[0] == name:
                    ExtensionServer[i] = name + " : "+etat
                    find = True
            if find:
                textF = "\n".join(ExtensionServer)
                with open("serveurs/"+ctx.message.server.name+"/modules.txt", "w") as fichier:
                    fichier.write(textF)
                if etat == "True":
                    await self.bot.say("Le module "+name+" est désormais activé")
                else:
                    await self.bot.say("Le module "+name+" est désormais désactivé")
            else:
                await self.bot.say("Le module "+name+" est inconnu")

    @config.command(pass_context=True)
    async def SpeechChannel(self, ctx, name = "None"):
        if name == "None":
            await self.bot.say("Usage : !set SpeechChannel <nom>")
        elif name == "OFF":
            with open("serveurs/"+ctx.message.server.name+"/config.txt", "r") as fichier:
                texteList = fichier.read().split("\n")
            texteList[1] = "xopzkpdkpk"
            textF = "\n".join(texteList)
            with open("serveurs/"+ctx.message.server.name+"/config.txt", "w") as fichier:
                fichier.write(textF)
            await self.bot.say("Le channel de speech est désormais désactivé")
        else:
            find = False
            for i in ctx.message.server.channels:
                if i.type == discord.ChannelType.text and i.name == name:
                    try:
                        await self.bot.send_message(i, "Activation de la fonction 'bot_speech' dans ce channel")
                        find = True
                    except:
                        find= False
            if find:
                with open("serveurs/"+ctx.message.server.name+"/config.txt", "r") as fichier:
                    texteList = fichier.read().split("\n")
                texteList[1] = name
                textF = "\n".join(texteList)
                with open("serveurs/"+ctx.message.server.name+"/config.txt", "w") as fichier:
                    fichier.write(textF)
                await self.bot.say("Le channel de speech est désormais : "+name)
            else:
                await self.bot.say("Ce channel n'existe pas, n'est pas textuel ou le bot n'a pas la permission d'y écrire des messages")

def setup(bot):
    bot.add_cog(Config(bot))
