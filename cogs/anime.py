from discord.ext import commands
import asyncio, discord, requests
from xml.etree import ElementTree
from cogs.futils import checkModuleSet

class Anime:
    """Commandes animes et manga."""

    def __init__(self, bot):
        self.bot = bot
        self.username = "LavaPower"
        self.mdp = "mamaju84"

    @commands.command()
    @commands.check(checkModuleSet)
    async def anime(self, *nom):
        nom = ' '.join(nom)
        if nom == "":
            await self.bot.say("Usage : !anime <nom>")
        else:
            titre = nom.replace(" ", "+")
            url = "https://myanimelist.net/api/anime/search.xml?q="+titre
            r = requests.get(url, auth=(self.username, self.mdp))
            try:
                tree = ElementTree.fromstring(r.content)
                anime = discord.Embed(title=nom+" - Anime", description="Informations de "+nom, colour = 0x2ecc71)
                anime.set_footer(text="Dislo - Anime System",icon_url="https://cdn4.iconfinder.com/data/icons/meBaze-Freebies/512/info.png")
                anime.set_thumbnail(url = tree[0][11].text)
                anime.add_field(name = "Titre", value = tree[0][1].text, inline = False)
                anime.add_field(name = "Titre Anglais", value = tree[0][2].text, inline = False)
                anime.add_field(name = "Nombre Episode", value = tree[0][4].text, inline = False)
                anime.add_field(name = "Note", value = tree[0][5].text, inline = False)
                anime.add_field(name = "Status", value = tree[0][7].text, inline = False)
                anime.add_field(name = "Début le", value = tree[0][8].text, inline = False)
                anime.add_field(name = "Fin le", value = tree[0][9].text, inline = False)
                anime.add_field(name = "Lien", value = "https://myanimelist.net/anime/"+tree[0][0].text, inline = False)
                await self.bot.say(embed=anime)
            except:
                await self.bot.say("Cet anime n'existe pas ou il y a trop d'informations.")
        
    @commands.command()
    @commands.check(checkModuleSet)
    async def manga(self, *nom):
        nom = ' '.join(nom)
        if nom == "":
            await self.bot.say("Usage : !manga <nom>")
        else:
            titre = nom.replace(" ", "+")
            url = "https://myanimelist.net/api/manga/search.xml?q="+titre
            r = requests.get(url, auth=(self.username, self.mdp))
            try:
                tree = ElementTree.fromstring(r.content)
                anime = discord.Embed(title=nom+" - Manga", description="Informations de "+nom, colour = 0x2ecc71)
                anime.set_footer(text="Dislo - Manga System",icon_url="https://cdn4.iconfinder.com/data/icons/meBaze-Freebies/512/info.png")
                anime.set_thumbnail(url = tree[0][12].text)
                anime.add_field(name = "Titre", value = tree[0][1].text, inline = False)
                anime.add_field(name = "Titre Anglais", value = tree[0][2].text, inline = False)
                anime.add_field(name = "Nombre Chapitre", value = tree[0][4].text, inline = False)
                anime.add_field(name = "Nombre Volume", value = tree[0][5].text, inline = False)
                anime.add_field(name = "Note", value = tree[0][6].text, inline = False)
                anime.add_field(name = "Status", value = tree[0][8].text, inline = False)
                anime.add_field(name = "Début le", value = tree[0][9].text, inline = False)
                anime.add_field(name = "Fin le", value = tree[0][10].text, inline = False)
                anime.add_field(name = "Lien", value = "https://myanimelist.net/manga/"+tree[0][0].text, inline = False)
                await self.bot.say(embed=anime)
            except:
                await self.bot.say("Ce manga n'existe pas ou il y a trop d'informations.")
   
def setup(bot):
    bot.add_cog(Anime(bot))
