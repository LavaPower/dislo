from discord.ext import commands
import asyncio, discord, os
from numpy import *
import matplotlib
matplotlib.use('Agg')
from matplotlib.pyplot import *
from cogs.futils import checkModuleSet

class Maths:
    """Commandes utilitaires."""

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.check(checkModuleSet)
    async def calcul(self, *calcul):
        calcul = ' '.join(calcul)
        if calcul == "":
            await self.bot.say("Usage : !calcul <calcul>")
        else:
            try:
                result = eval(calcul)
            except:
                await self.bot.say("Calcul inconnu : "+calcul)
            else:
                await self.bot.say("Résultat : "+str(result))

    @commands.command(pass_context=True)
    @commands.check(checkModuleSet)
    async def fonction(self, ctx, formule = "None", borne = "None", point="None"):
        if formule == "None" or borne == "None" or point=="None":
            await self.bot.say("Usage : !fonction <formule> <min|max> <nbPoint>")
        else:
            try:
                mini = float(borne.split("|")[0])
                maxi = float(borne.split("|")[1])
            except:
                await self.bot.say("Erreur : Bornes inconnues")
                await self.bot.say("Usage : !fonction <formule> <min|max> <nbPoint>")
            else:
                try:
                    point = int(point)
                except:
                    await self.bot.say("Erreur : Nombre de point inconnu")
                    await self.bot.say("Usage : !fonction <formule> <min|max> <nbPoint>")
                else:
                    try:
                        x = linspace(mini, maxi, point)
                        y = eval(formule)
                    except:
                        await self.bot.say("Erreur : Formule inconnue")
                        await self.bot.say("Usage : !fonction <formule> <min|max> <nbPoint>")
                    else:
                        ax = gca()
                        ax.spines['right'].set_color('none')
                        ax.spines['top'].set_color('none')
                        ax.xaxis.set_ticks_position('bottom')
                        ax.spines['bottom'].set_position(('data',0))
                        ax.yaxis.set_ticks_position('left')
                        ax.spines['left'].set_position(('data',0))
                        title(formule)
                        xlabel('X')
                        ylabel('Y')
                        plot(x, y)
                        savefig('plot.png')
                        gcf().clear()
                        await self.bot.send_file(ctx.message.channel, 'plot.png')
                        os.remove('plot.png')
                              
def setup(bot):
    bot.add_cog(Maths(bot))
