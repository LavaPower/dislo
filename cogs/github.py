from discord.ext import commands
import asyncio, discord, requests, json
from cogs.futils import checkModuleSet

class Github:
    """Commandes github."""

    def __init__(self, bot):
        self.bot = bot

    @commands.group(pass_context=True)
    @commands.check(checkModuleSet)
    async def github(self, ctx):
        if ctx.invoked_subcommand is None:
            aide = discord.Embed(title="Dislo - Github System", description="Système Github de Dislo", colour = 0x2ecc71)
            aide.set_footer(text="Dislo - Github System",icon_url="https://cdn4.iconfinder.com/data/icons/meBaze-Freebies/512/info.png")
            aide.add_field(name = "- !github help", value = "```Commande actuelle```", inline = False)
            aide.add_field(name = "- !github user <nom>", value = "```Informations sur un utilisateur```", inline = False)
            aide.add_field(name = "- !github repos <nom>", value = "```Repos d'un utilisateur```", inline = False)
            aide.add_field(name = "- !github repo <user> <repo>", value = "```Informations sur un repo d'un utilisateur```", inline = False)
            aide.add_field(name = "- !github followers <nom>", value = "```Followers d'un utilisateur```", inline = False)
            aide.add_field(name = "- !github following <nom>", value = "```Personnes suivies par un utilisateur```", inline = False)
            await self.bot.say(embed=aide)

    @github.command()
    async def user(self, name = "None"):
        if name == "None":
            await self.bot.say("Usage : !github user <nom>")
        else:
            url = "https://api.github.com/users/"+name
            r = requests.get(url)
            infos = r.json()
            try:
                if infos["message"] == "Not Found":
                    await self.bot.say("Utilisateur inconnu.")
                else:
                    0/0
            except:
                info = discord.Embed(title=name+" - Github", description="Informations de "+name, colour = 0x2ecc71)
                info.set_footer(text="Dislo - Github System",icon_url=infos["avatar_url"])
                info.add_field(name = "Informations", value = infos["bio"], inline = False)
                info.add_field(name = "Créé le", value = infos["created_at"][:10], inline = False)
                info.add_field(name = "Dernière update le", value = infos["updated_at"][:10], inline = False)
                info.add_field(name = "Nombre de followers", value = infos["followers"], inline = False)
                info.add_field(name = "Nombre de personnes suivies", value = infos["following"], inline = False)
                info.add_field(name = "Nombre de repositories publiques", value = infos["public_repos"], inline = False)
                info.add_field(name = "Liste des repositories", value = "!github repos "+name, inline = False)
                info.add_field(name = "Liste des followers", value = "!github followers "+name, inline = False)
                info.add_field(name = "Liste des personnes suivies", value = "!github following "+name, inline = False)
                info.add_field(name = "Lien github", value = infos["html_url"], inline = False)
                await self.bot.say(embed=info)

    @github.command()
    async def repo(self, user = "None", name = "None"):
        if name == "None" or user == "None":
            await self.bot.say("Usage : !github repo <user> <repo>")
        else:
            url = "https://api.github.com/repos/"+user+"/"+name
            r = requests.get(url)
            infos = r.json()
            try:
                if infos["message"] == "Not Found":
                    await self.bot.say("Utilisateur ou repo inconnu.")
                else:
                    0/0
            except:
                name, owner, created, updated, language, forks, subs, url = infos["name"], infos["owner"]["login"], infos["created_at"][:10], infos["updated_at"][:10], infos["language"], infos["forks_count"], infos["subscribers_count"], infos["html_url"]
                repo = discord.Embed(title=name+" - Github", description="Informations de "+name, colour = 0x2ecc71)
                repo.set_footer(text="Dislo - Github System",icon_url=infos["owner"]["avatar_url"])
                repo.add_field(name = "Nom", value = name, inline = False)
                repo.add_field(name = "Créateur", value = owner, inline = False)
                repo.add_field(name = "Création", value = created, inline = False)
                repo.add_field(name = "Update", value = updated, inline = False)
                repo.add_field(name = "Langage", value = language, inline = False)
                repo.add_field(name = "Forks", value = forks, inline = False)
                repo.add_field(name = "Stars", value = subs, inline = False)
                repo.add_field(name = "Lien", value = url, inline = False)
                await self.bot.say(embed=repo)

    @github.command()
    async def repos(self, name = "None"):
        if name == "None":
            await self.bot.say("Usage : !github repos <nom>")
        else:
            url = "https://api.github.com/users/"+name+"/repos"
            r = requests.get(url)
            infos = r.json()
            try:
                if infos["message"] == "Not Found":
                    await self.bot.say("Utilisateur inconnu.")
                else:
                    0/0
            except:
                texte = "Voici la liste des repos de "+name+":\n"
                for i in infos:
                    texte += "- "+i["name"]+"\n"
                texte += "Pour plus d'informations : !github repo "+name+" <repo>"
                await self.bot.say(texte)

    @github.command()
    async def followers(self, name = "None"):
        if name == "None":
            await self.bot.say("Usage : !github followers <nom>")
        else:
            url = "https://api.github.com/users/"+name+"/followers"
            r = requests.get(url)
            infos = r.json()
            try:
                if infos["message"] == "Not Found":
                    await self.bot.say("Utilisateur inconnu.")
                else:
                    0/0
            except:
                texte = "Voici la liste des followers de "+name+":\n"
                for i in infos:
                    texte += "- "+i["login"]+"\n"
                texte += "Pour plus d'informations : !github user <nom>"
                await self.bot.say(texte)

    @github.command()
    async def following(self, name = "None"):
        if name == "None":
            await self.bot.say("Usage : !github following <nom>")
        else:
            url = "https://api.github.com/users/"+name+"/following"
            r = requests.get(url)
            infos = r.json()
            try:
                if infos["message"] == "Not Found":
                    await self.bot.say("Utilisateur inconnu.")
                else:
                    0/0
            except:
                texte = "Voici la liste des personnes suivies par "+name+":\n"
                for i in infos:
                    texte += "- "+i["login"]+"\n"
                texte += "Pour plus d'informations : !github user <nom>"
                await self.bot.say(texte)

   
def setup(bot):
    bot.add_cog(Github(bot))

